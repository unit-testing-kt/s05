const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	// app.get('/', (req, res) => {
	// 	return res.send({'data': {} });
	// });

	// app.get('/rates', (req, res) => {
	// 	return res.status(200).send({
	// 		rates: exchangeRates
	// 	});
	// })


	app.post('/currency', (req, res) => {
		// Check if post /currency is running
		if(Object.keys(req.body).length === 0){
		 	return res.status(200).send({
				message: "endpoint running"
			})
		}
		// Check if post /currency returns status 400 if name is missing
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter NAME'
			})
		}
		// Check if post /currency returns status 400 if name is not a string
		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error': 'Bad Request - NAME has to be a string'
			})
		}
		// Check if post /currency returns status 400 if name is empty
		if(req.body.name === ""){
			return res.status(400).send({
				'error': 'Bad Request - NAME cannot be an empty string'
			})
		}
		// Check if post /currency returns status 400 if ex is missing
		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter EX'
			})	
		}
		// Check if post /currency returns status 400 if ex is not an object
		if(typeof req.body.ex !== "object"){
			return res.status(400).send({
				'error': 'Bad Request - EX should be an object'
			})	
		}
		// Check if post /currency returns status 400 if ex is empty
		if(Object.keys(req.body.ex).length === 0){
			return res.status(400).send({
				'error': 'Bad Request - EX should have currencies'
			})	
		}
		// Check if post /currency returns status 400 if alias is missing
		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error': 'Bad Request - missing required parameter ALIAS'
			})	
		}
		// Check if post /currency returns status 400 if alias is not an string
		if(typeof req.body.alias !== 'string'){
			return res.status(400).send({
				'error': 'Bad Request - ALIAS has to be a string'
			})	
		}
		// Check if post /currency returns status 400 if alias is empty
		if(req.body.alias === ''){
			return res.status(400).send({
				'error': 'Bad Request - ALIAS cannot be an empty string'
			})	
		}
		// Check if post /currency returns status 200 if all fields are complete and there are no dupicates
		if(req.body.hasOwnProperty('alias') && req.body.hasOwnProperty('name') && req.body.hasOwnProperty('ex')){
			// Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias
			if(exchangeRates.hasOwnProperty(req.body.alias)){
				return res.status(400).send({
					message: 'duplicate found'
				})
			}
			return res.status(200).send({
				message: 'received successfully'
			})
		}
		
	})
}
